# README #

This code base is a draft implementation of the following:
* Simple screen with activity & 2 fragments with navigation
* MVVM, databinding
* Dagger DI
* RxAndroid
* Basic RecyclerView + Adapter
* Tests for VM and command

From business perspective it shows a screen with a list of products.
On item click the product is getting removed.
"Next" button click opens up other placeholder fragment.
"Previous" button click opens up the initial screen again.