package com.example.alesiademo

import com.example.alesiademo.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class App : DaggerApplication() {

    private val applicationInjector = DaggerAppComponent.builder().application(this).build()
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = applicationInjector

    companion object {
        lateinit var app: App

//        fun getApp(): App {
//            return App.app
//        }

    }

    override fun onCreate() {
        super.onCreate()
        app = this
    }


}