package com.example.alesiademo.di.component

import android.app.Application
import com.example.alesiademo.App
import com.example.alesiademo.di.modules.AppModule
import com.example.alesiademo.di.modules.MainActivityModule
import com.example.alesiademo.di.modules.SchedulersModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        SchedulersModule::class,
//        RepositoriesModule::class,
//        ServicesModule::class,
        AppModule::class,
        MainActivityModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(app: App)
}