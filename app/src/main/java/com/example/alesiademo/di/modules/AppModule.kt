package com.example.alesiademo.di.modules

import com.example.alesiademo.di.ViewModelModule
import com.example.alesiademo.di.qualifiers.GetProducts
import com.example.alesiademo.domain.products.GetProductsCommand
import com.example.alesiademo.domain.products.LocationList
import dagger.Binds
import dagger.Module
import io.reactivex.Observable

@Module(includes = [ViewModelModule::class])
abstract class AppModule {

    @GetProducts
    @Binds
    abstract fun providesGetProducts(command: GetProductsCommand?): Function0<Observable<LocationList?>?>?

}