package com.example.alesiademo.di.modules

import com.example.alesiademo.di.qualifiers.IoScheduler
import com.example.alesiademo.domain.products.LocationService
import com.example.alesiademo.domain.products.LocationsRepository
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Singleton

@Module
object RepositoriesModule {

    @Singleton
    @Provides
    @JvmStatic
    fun providesLocationsRepository(locationService: LocationService,
                                     @IoScheduler ioSchedular: Scheduler
    ): LocationsRepository {
        return LocationsRepository(locationService, ioSchedular)
    }
}