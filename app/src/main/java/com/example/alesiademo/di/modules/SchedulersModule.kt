package com.example.alesiademo.di.modules

import com.example.alesiademo.di.qualifiers.IoScheduler
import com.example.alesiademo.di.qualifiers.MainThreadScheduler
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module
object SchedulersModule {

    @Singleton
    @MainThreadScheduler
    @Provides
    @JvmStatic
    fun providesMainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @Singleton
    @IoScheduler
    @Provides
    @JvmStatic
    fun providesIoScheduler(): Scheduler = Schedulers.from(Executors.newSingleThreadExecutor())
}