package com.example.alesiademo.di.modules

import com.example.alesiademo.di.qualifiers.Api
import com.example.alesiademo.domain.products.LocationService
import com.example.alesiademo.domain.products.StubLocationService
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
object ServicesModule {

    //This is for network, we gonna do stub for now
//    @Provides
//    @JvmStatic
//    fun providesLocationService(@Api retrofit: Retrofit): LocationService {
//        return retrofit.create(LocationService::class.java)
//    }

    @Provides
    @JvmStatic
    fun providesLocationService(): LocationService = StubLocationService

    //TODO net module
    @Provides
    @Singleton
    @Api
    fun providesApiRetrofit(gson: Gson, @Api okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("placeholder")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun providesGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
        //todo
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    @Api
    fun providesOkHttpClient(): OkHttpClient {
        val okHttpClient = OkHttpClient.Builder()
        //todo
        return okHttpClient.build()
    }
}