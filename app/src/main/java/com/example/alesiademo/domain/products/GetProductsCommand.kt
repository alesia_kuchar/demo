package com.example.alesiademo.domain.products

import com.example.alesiademo.entities.LocationListEntity
import io.reactivex.Observable
import javax.inject.Inject

class GetProductsCommand @Inject constructor(
    private val locationsRepository: LocationsRepository
) : () -> @JvmSuppressWildcards Observable<LocationList> {

    override fun invoke(): Observable<LocationList> {
//        return Observable.just(
////            listOf(
////                "Product one from command",
////                "Product two from command",
////                "Product three from command",
////                "Product four from command",
////                "Product five from command"
////            )
////        )
        locationsRepository.loadLocations()
        return locationsRepository.locations.map {
            LocationList(locations = it.locations.map { locationEntity ->
                DemoLocation(
                    name = locationEntity.name,
                    lat = locationEntity.lat,
                    lng = locationEntity.lng
                )
            })
        }
    }
}