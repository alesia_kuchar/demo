package com.example.alesiademo.domain.products

data class LocationList(
    val locations: List<DemoLocation>
)

data class DemoLocation(
    val name: String,
    val lat: Long,
    val lng: Long
)