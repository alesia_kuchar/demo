package com.example.alesiademo.domain.products

import com.example.alesiademo.App
import com.example.alesiademo.entities.LocationListEntity
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.Single
import retrofit2.http.GET
import java.io.BufferedReader
import java.io.File
import java.io.FileReader


interface LocationService {

    @GET("url/placeholder")
    fun getLocations(): Single<LocationListEntity>
}

object StubLocationService : LocationService {

    override fun getLocations(): Single<LocationListEntity> {
//        return serviceForStubSingle<LocationListEntity>("locations.json").delay(2, TimeUnit.SECONDS)

        val file = File(App.app.filesDir, "locations.json")
        val fileReader = FileReader(file)
        val bufferedReader = BufferedReader(fileReader)
        val stringBuilder = StringBuilder()
        var line: String = bufferedReader.readLine()
        while (line != null) {
            stringBuilder.append(line).append("\n")
            line = bufferedReader.readLine()
        }
        bufferedReader.close()
        // This response will have Json Format String
        val response = stringBuilder.toString()

        val stub: LocationListEntity = buildGson().fromJson(response, LocationListEntity::class.java)

        return Single.just(stub)
    }

    private fun buildGson() : Gson
    {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY);
//        gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
//            public Date deserialize(
//                JsonElement json,
//                Type typeOfT,
//                JsonDeserializationContext context
//            ) throws JsonParseException {
//                return new Date (json.getAsJsonPrimitive().getAsLong());
//            }
//        });
        return gsonBuilder.create();
    }

//    inline fun <reified T> serviceForStubSingle(file: String): Single<T> {
//        val stubConfig: T? = StubHe
//    }

//    inline fun <reified T> serviceForStubSingle(file: String): Single<T> {
//        val stubConfig: T? = loadStub(T::class.java, file)
//        return if (stubConfig != null) {
//            Single.just(stubConfig).subscribeOn(Schedulers.newThread())
//        } else {
//            Observable.error<T>(RuntimeException("Could Not load Stub Config for ${T::class.java.canonicalName}")).subscribeOn(Schedulers.io())
//        }
//    }

//    fun <T> loadStub(clazz: Class<T>?, fileName: String?): T? {
//        if (sStubCache.get(fileName) != null) {
//            return sStubCache.get(fileName)
//        }
//        if (sGson == null) {
//            buildGson()
//        }
//        val stubsDir: File = getStubsDirectory()
//        val file = File(stubsDir, fileName)
//        var `is`: InputStream? = null
//        try {
//            `is` = FileInputStream(file)
//            val size: Int = `is`.available()
//            val buffer = ByteArray(size)
//            `is`.read(buffer)
//            val json = String(buffer, "UTF-8")
//            val stub: T = sGson.fromJson(json, clazz)
//            if (stub != null) {
//                sStubCache.put(fileName, stub)
//            }
//            return stub
//        } catch (e: Throwable) {
//            Log.e("StubHelper", e.message, e)
//        } finally {
//            if (`is` != null) {
//                try {
//                    `is`.close()
//                } catch (e: IOException) {
//                    Log.e("StubHelper", e.getMessage(), e)
//                }
//            }
//        }
//        return null
//    }

}
