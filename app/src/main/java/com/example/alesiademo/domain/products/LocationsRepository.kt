package com.example.alesiademo.domain.products

import com.example.alesiademo.entities.LocationListEntity
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.subjects.BehaviorSubject

class LocationsRepository constructor(
    locationService: LocationService,
    private val ioSchedular: Scheduler
) {

    private val loadLocationsSubject = BehaviorSubject.create<Unit>()
    val locations: Observable<LocationListEntity>

    init {
        locations = loadLocationsSubject.switchMapSingle {
            locationService.getLocations().subscribeOn(ioSchedular)
        }.toHotObservable()
    }

    fun loadLocations() = loadLocationsSubject.onNext(Unit)
}

//todo util ext
fun <T> Observable<T>.toHotObservable(): Observable<T> {
    val behavior = BehaviorSubject.create<T>()
    return this.compose {
        it.subscribe(behavior)
        behavior.hide()
    }
}