package com.example.alesiademo.domain.products

import android.util.Log
import com.example.alesiademo.App
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


//public class StubHelper {
////    private static HashMap<String, Object> sStubCache = new HashMap<>();
//    private var sGson : Gson =  null
//
//    public static <T> T loadStub(Class<T> clazz, String fileName)
//    {
////        if (sStubCache.get(fileName) != null) {
////            //noinspection unchecked
////            return (T) sStubCache . get (fileName);
////        }
//        if (sGson == null) {
//            buildGson()
//        }
//        val stubsDir = getStubsDirectory();
//        File file = new File(stubsDir, fileName);
//        InputStream is = null;
//        try {
//            is = new FileInputStream(file);
//            int size = is.available();
//            byte[] buffer = new byte[size];
//            is.read(buffer);
//            String json = new String(buffer, "UTF-8");
//            T stub = sGson . fromJson (json, clazz);
//            if (stub != null) {
//                sStubCache.put(fileName, stub);
//            }
//            return stub;
//        } catch (Throwable e) {
//            Log.e("StubHelper", e.getMessage(), e);
//        } finally {
//            if ( is != null) {
//                try {
//                    is.close();
//                } catch (IOException e) {
//                    Log.e("StubHelper", e.getMessage(), e);
//                }
//            }
//        }
//        return null;
//    }
//
//    public static <T> ArrayList<T> loadStubArray(Class<T> clazz, String fileName)
//    {
//        if (sStubCache.get(fileName) != null) {
//            //noinspection unchecked
//            return (ArrayList<T>) sStubCache . get (fileName);
//        }
//        if (sGson == null) {
//            buildGson();
//        }
//        File stubsDir = getStubsDirectory ();
//        File file = new File(stubsDir, fileName);
//        InputStream is = null;
//        try {
//            is = new FileInputStream(file);
//            InputStreamReader isr = new InputStreamReader(is, "UTF-8");
//            JsonReader reader = new JsonReader(isr);
//            ArrayList<T> stubArray = new ArrayList<>();
//            reader.beginArray();
//            while (reader.hasNext()) {
//                T stub = sGson . fromJson (reader, clazz);
//                if (stub != null) {
//                    stubArray.add(stub);
//                }
//            }
//            reader.endArray();
//            reader.close();
//            if (stubArray.size() > 0) {
//                sStubCache.put(fileName, stubArray);
//            }
//            return stubArray;
//        } catch (Exception e) {
//            Log.e("StubHelper", e.getMessage(), e);
//        } finally {
//            if ( is != null) {
//                try {
//                    is.close();
//                } catch (IOException e) {
//                    Log.e("StubHelper", e.getMessage(), e);
//                }
//            }
//        }
//        return null;
//    }
//
//    private fun buildGson()
//    {
//        val gsonBuilder = GsonBuilder()
//        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY);
////        gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
////            public Date deserialize(
////                JsonElement json,
////                Type typeOfT,
////                JsonDeserializationContext context
////            ) throws JsonParseException {
////                return new Date (json.getAsJsonPrimitive().getAsLong());
////            }
////        });
//        sGson = gsonBuilder.create();
//    }
//
//    public static Type getParameterUpperBound(int index, ParameterizedType type)
//    {
//        Type[] types = type . getActualTypeArguments ();
//        if (index < 0 || index >= types.length) {
//            throw new IllegalArgumentException (
//                    "Index " + index + " not in range [0," + types.length + ") for " + type);
//        }
//        Type paramType = types [index];
//        if (paramType instanceof WildcardType) {
//            return ((WildcardType) paramType).getUpperBounds()[0];
//        }
//        return paramType;
//    }
//
//    public static Buffer readStubFile(String fileName)
//    {
//        if (sStubCache.get(fileName) != null) {
//            //noinspection unchecked
//            return (Buffer) sStubCache . get (fileName);
//        }
//        File stubsDir = getStubsDirectory ();
//        File file = new File(stubsDir, fileName);
//        InputStream is = null;
//        try {
//            is = new FileInputStream(file);
//            return new Buffer ().readFrom(is);
//
//        } catch (Throwable e) {
//            Log.e("StubHelper", e.getMessage(), e);
//        } finally {
//            if ( is != null) {
//                try {
//                    is.close();
//                } catch (IOException e) {
//                    Log.e("StubHelper", e.getMessage(), e);
//                }
//            }
//        }
//        return null;
//
//        public static void copyStubsToDisk(Context context, @NonNull String version) {
//        Log.d("STUB", "STUBVERSION " + version);
//        AssetManager assetManager = context.getAssets();
//        boolean copiedAnything = false;
//        //Copy default stubs
//        try {
//            String assetsDir = "stub";
//            String[] assets = assetManager.list(assetsDir);
//            if (assets.length == 0) {
//                return;
//            }
//            for (String asset : assets) {
//                Log.d("ORIGINAL STUBS", asset);
//            }
//            File stubsDir = getStubsDirectory();
//            for (String file : assets) {
//                File f = new File(stubsDir, file);
//                //if file is not already copied over and ends with json.
//                if (!f.exists()) {
//                    copyFile(context, assetsDir, file);
//                    copiedAnything = true;
//                }
//            }
//        } catch (Exception e) {
//            Log.e("StubHelper", e.getMessage(), e);
//        }
//
//        if (TextUtils.isEmpty(version) || !copiedAnything) {
//            //No version specific override
//            //Already copied everything, this is not a clean run
//            return;
//        }
//
//        //Override default stubs with version
//        try {
//            String assetsDir = "stub" + version;
//            String[] assets = assetManager.list(assetsDir);
//            if (assets.length == 0) {
//                return;
//            }
//            for (String asset : assets) {
//                Log.d("VERSION STUBS", asset);
//            }
//            File stubsDir = getStubsDirectory();
//            for (String file : assets) {
//                File f = new File(stubsDir, file);
//                if (f.exists() && file.endsWith(".json") || file.endsWith(".txt")) {
//                    f.delete();
//                }
//                //if file is not already copied over and ends with json.
//                if (file.endsWith(".json") || file.endsWith(".txt")) {
//                    copyFile(context, assetsDir, file);
//                }
//            }
//        } catch (Exception e) {
//            Log.e("StubHelper", e.getMessage(), e);
//        }
//    }
//
//        private static void copyFile(Context context, String assetDir, String filename) {
//        AssetManager assetManager = context.getAssets();
//
//        InputStream in;
//        OutputStream out;
//        File stubsDir = getStubsDirectory();
//        File newFileName = null;
//        try {
//            Log.i("tag", "copyFile() " + assetDir + "/" + filename);
//            in = assetManager.open(assetDir + "/" + filename);
//
//            if (filename.endsWith(".jpg")) // extension was added to avoid compression on APK file
//                newFileName = new File(stubsDir, filename.substring(0, filename.length() - 4));
//            else
//            newFileName = new File(stubsDir, filename);
//            out = new FileOutputStream(newFileName, false); //append
//
//            byte[] buffer = new byte[1024];
//            int read;
//            while ((read = in.read(buffer)) != -1) {
//                out.write(buffer, 0, read);
//            }
//            in.close();
//            out.flush();
//            out.close();
//        } catch (Exception e) {
//            Log.e("tag", "Exception in copyFile() of " + newFileName);
//            Log.e("tag", "Exception in copyFile() " + e.toString());
//        }
//
//    }
//
//        private val getStubsDirectory(): File {
//            val stubsDir = File(App.app.filesDir, "stubs");
//            stubsDir = new File(stubsDir, STUBS_VERSION);
//            if (!stubsDir.exists()) {
//                stubsDir.mkdirs();
//            }
//            return stubsDir;
//        }
//
//        public static void clearStubs() {
//            File stubsDir = getStubsDirectory();
//            File[] files = stubsDir.listFiles();
//            for (File f : files) {
//            f.delete();
//        }
//            sStubCache.clear();
//        }
//
//    }