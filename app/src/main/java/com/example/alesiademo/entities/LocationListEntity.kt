package com.example.alesiademo.entities

data class LocationListEntity(
    val locations: List<DemoLocationEntity>,
    val updated: String
)

data class DemoLocationEntity(
    val name: String,
    val lat: Long,
    val lng: Long
)