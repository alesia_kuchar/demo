package com.example.alesiademo.presentation

import android.util.Log
import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    @CallSuper
    override fun onCleared() {
        compositeDisposable.clear()
    }

    private fun Disposable.addToComposite() {
        addSubscription(this)
    }

    fun <T> Observable<T>.subscribe(
        onCompleteDelegate: (() -> Unit)? = null,
        onErrorDelegate: ((Throwable) -> Unit)? = null,
        onNextDelegate: ((T) -> Unit)? = null
    ): Disposable {
        val observer = object : DemoObserver<T>() {
            override fun onNext(t: T) {
                super.onNext(t)
                onNextDelegate?.invoke(t)
            }

            override fun onComplete() {
                super.onComplete()
                onCompleteDelegate?.invoke()
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                onErrorDelegate?.invoke(e)
            }
        }
        return subscribe(
            { observer.onNext(it) },
            { observer.onError(it) },
            { observer.onComplete() }).also {
            it.addToComposite()
        }
    }

    fun <T> Single<T>.subscribe(
        onErrorDelegate: ((Throwable) -> Unit)? = null,
        onSuccessDelegate: ((T) -> Unit)? = null
    ): Disposable {
        val observer = object : DemoSingleObserver<T>() {
            override fun onSuccess(t: T) {
                super.onSuccess(t)
                onSuccessDelegate?.invoke(t)
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                onErrorDelegate?.invoke(e)
            }
        }
        return subscribe({ observer.onSuccess(it) }, { observer.onError(it) }).also {
            it.addToComposite()
        }
    }

    fun Completable.subscribe(
        onErrorDelegate: ((Throwable) -> Unit)? = null,
        onCompleteDelegate: (() -> Unit)? = null
    ): Disposable {
        val observer = object : DemoCompletableObserver() {
            override fun onComplete() {
                super.onComplete()
                onCompleteDelegate?.invoke()
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                onErrorDelegate?.invoke(e)
            }
        }
        return subscribe({ observer.onComplete() }, { observer.onError(it) }).also {
            it.addToComposite()
        }
    }

    private fun addSubscription(disposable: Disposable?) {
        if (disposable != null) compositeDisposable.add(disposable)
    }

}

abstract class DemoObserver<T> : Observer<T> {

    companion object {
        private const val TAG = "DemoObserver"
    }

    override fun onComplete() {}

    override fun onNext(t: T) {}

    @CallSuper
    override fun onError(e: Throwable) {
        Log.d(TAG, e.message.orEmpty())
    }

    override fun onSubscribe(d: Disposable) {}
}

abstract class DemoSingleObserver<T> : SingleObserver<T> {

    companion object {
        private const val TAG = "DemoSingleObserver"
    }

    override fun onSuccess(t: T) {}

    @CallSuper
    override fun onError(e: Throwable) {
        Log.d(TAG, e.message.orEmpty())
    }

    override fun onSubscribe(d: Disposable) {}
}

abstract class DemoCompletableObserver : CompletableObserver {

    companion object {
        private const val TAG = "DemoCompletableObserver"
    }

    override fun onComplete() {}

    final override fun onSubscribe(d: Disposable) {
        onSubscribe()
    }

    open fun onSubscribe() {}

    @CallSuper
    override fun onError(e: Throwable) {
        Log.d(TAG, e.message.orEmpty())
    }
}