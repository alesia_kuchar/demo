package com.example.alesiademo.presentation.products

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.alesiademo.R
import com.example.alesiademo.databinding.ItemProductBinding

class ProductsAdapter(private val context: Context, private val inflater: LayoutInflater) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var products: List<String> = emptyList()
        set(value) {
            val diff = DiffUtil.calculateDiff(ItemsDiff(field, value))
            field = value
            diff.dispatchUpdatesTo(this)
        }

    var delete: ((String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ItemProductBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_product,
            parent,
            false
        )
        return ItemViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemViewHolder).bind(products[position])
    }

    inner class ItemViewHolder(private val binding: ItemProductBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(product: String) {
            binding.product = product
            binding.onClick = View.OnClickListener { delete?.invoke(product) }
        }
    }
}