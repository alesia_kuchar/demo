package com.example.alesiademo.presentation.products

import androidx.databinding.BindingAdapter

object ProductsBindings {

    @JvmStatic
    @BindingAdapter("products")
    fun bindItems(recycler: ProductsRecycler, items: List<String>?){
        items?.let { recycler.products = it }
    }
}