package com.example.alesiademo.presentation.products

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.alesiademo.PropertyDelegate

class ProductsRecycler : RecyclerView {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    private val firstAdapter = ProductsAdapter(context, LayoutInflater.from(context))

    var products: List<String> by PropertyDelegate(firstAdapter::products)
    var deleteItem: ((String) -> Unit)? by PropertyDelegate(firstAdapter::delete)

    init {
        adapter = firstAdapter
        layoutManager = LinearLayoutManager(context)
    }
}