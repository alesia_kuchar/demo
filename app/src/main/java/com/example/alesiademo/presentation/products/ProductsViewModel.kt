package com.example.alesiademo.presentation.products

import androidx.lifecycle.MutableLiveData
import com.example.alesiademo.di.qualifiers.GetProducts
import com.example.alesiademo.di.qualifiers.MainThreadScheduler
import com.example.alesiademo.domain.products.LocationList
import com.example.alesiademo.entities.LocationListEntity
import com.example.alesiademo.presentation.BaseViewModel
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject

class ProductsViewModel @Inject constructor(
    @GetProducts private val getProducts: () -> @JvmSuppressWildcards Observable<LocationList>,
    @MainThreadScheduler private val mainThread: Scheduler
) : BaseViewModel() {

    val items = MutableLiveData<List<String>>()
    private var initialItems: List<String> = emptyList()

    val delete: (String) -> Unit = {
        val res = items.value?.toMutableList() ?: mutableListOf()
        val index = res.indexOf(it)
        if (index >= 0) {
            res.removeAt(index)
            items.value = res
        }
    }

    init {
        initialItems = listOf("First Product in the list", "Second Product in the list")

        getProducts().observeOn(mainThread).subscribe(
            onNextDelegate = {
                items.value = it.locations.map { it.name }
            }
        )
    }

    fun setupData() {
        items.value = initialItems
    }
}