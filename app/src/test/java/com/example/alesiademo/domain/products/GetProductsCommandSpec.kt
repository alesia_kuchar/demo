package com.example.alesiademo.domain.products

import io.reactivex.subscribers.TestSubscriber
import org.junit.Before
import org.junit.Test

class GetProductsCommandSpec {

    private lateinit var command: GetProductsCommand
    private lateinit var subscriber: TestSubscriber<List<String>>

    @Before
    fun setup() {
        command = GetProductsCommand()
        subscriber = TestSubscriber()
    }

    @Test
    fun `invoke() gets products`() {
        //given
        val result = listOf("product one command", "product two command")

        //when
        val subscriber = command().test()

        //then
        subscriber.assertNoErrors()
        subscriber.assertValue(result)
    }
}