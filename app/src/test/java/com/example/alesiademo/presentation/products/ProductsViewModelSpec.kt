package com.example.alesiademo.presentation.products

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ProductsViewModelSpec {

    @Rule
    fun rule() = InstantTaskExecutorRule()

    private lateinit var viewModel: ProductsViewModel
    private lateinit var getProducts: () -> Observable<List<String>>
    private lateinit var getProductsStream: BehaviorSubject<List<String>>

    private lateinit var lifecycle: LifecycleRegistry
    private lateinit var owner: LifecycleOwner

    @Before
    fun setup() {
        owner = LifecycleOwner { lifecycle }
        lifecycle = LifecycleRegistry(owner)
        lifecycle.currentState = Lifecycle.State.RESUMED

        getProducts = mockk()
        getProductsStream = BehaviorSubject.create()
        every { getProducts.invoke() } returns getProductsStream

    }

    @Test
    fun `getProducts initialises products`() {
        //given
        viewModel = ProductsViewModel(
            getProducts = getProducts,
            mainThread = Schedulers.single()
        )

        //when
        val products = listOf("a", "b", "c")
        getProductsStream.onNext(products)
        getProductsStream.onComplete()

        //then
        viewModel.items.value shouldEqual products
        verify(exactly = 1) { getProducts.invoke() }
    }
}